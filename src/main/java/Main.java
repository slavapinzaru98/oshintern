import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

public class Main {



    public static void main(String[] args) throws IOException, ParseException {

        JsonHandler jsonHandler = new JsonHandler();
        jsonHandler.initialization();
        jsonHandler.displayMenuOfProducts();

        System.out.printf("\n\tMenu for User \n");
        System.out.printf("select the product you want to buy :\n" +
                "to do this, select the row where the product is located\n ");
        System.out.printf("press the character\n");
        Scanner scanner = new Scanner(System.in);
        char c = scanner.next().charAt(0);
        int row = c - 97;

        System.out.printf("press the number\n");
        int column = scanner.nextInt();
        System.out.printf("great! You choose the\n");
        jsonHandler.displayChooseProduct(row, column);
    }


}
