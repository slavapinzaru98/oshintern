import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class JsonHandler {
    private final String filePath = "src\\main\\resources\\input.json";

    private JSONObject config;
    private JSONArray items;

    public void initialization() throws IOException, ParseException {
        FileReader reader = new FileReader(filePath);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
        this.config = (JSONObject) jsonObject.get("config");
        this.items = (JSONArray) jsonObject.get("items");
    }

    public void displayMenuOfProducts() {
        Long rows = (Long) config.get("rows");
        String s = (String) config.get("columns");
        char[] c = s.toCharArray();
        int columns = c[0] - 48;
        Iterator iterator = items.iterator();

        for (int i = 1; i <= columns; i++) {
            System.out.printf("%36s", "" + " -" + i + "-    ");
        }
        System.out.println();
        for (int i = 0; i < rows; i++) {
            char character = (char) ((char) 97 + i);
            System.out.print(character);
            for (int j = 0; j < columns; j++) {
                if (iterator.hasNext()) {
                    JSONObject innerObj = (JSONObject) iterator.next();
                    System.out.printf("%36s", "" + innerObj.get("name") + " " + innerObj.get("amount") + " " + innerObj.get("price"));
                } else
                    System.out.printf("%36s", "" + "  -----//-----  ");
            }
            System.out.println();
        }
    }

    public void displayChooseProduct(int rowOfProduct, int columnOfProduct) {
        String s = (String) config.get("columns");
        char[] c = s.toCharArray();
        int columns = c[0] - 48;
        for (int i = 0; i <= rowOfProduct * columns + columnOfProduct; i++) {
            if (i + 1 == rowOfProduct * columns + columnOfProduct)
                System.out.println("The" + i + "  " + items.get(i));
        }

    }
}
